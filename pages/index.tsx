import supabase from '../utils/supabase';

interface TagsModelInterface {
  id: number,
  created_at: Date,
  title: string,
  description: string,
  is_public: boolean
}
interface ListTagsInterface {
  tags: TagsModelInterface[]
}

export async function getStaticProps() {
  const { data: tags, error } = await supabase.from('tags').select('*');
  if (error) {
    throw new Error(error.message);
  }
  return {
    props: {
      tags,
    }
  }
}

export default function Home({ tags }: ListTagsInterface) {
  return (
    <div>
      <pre>{JSON.stringify(tags, null, 2)}</pre>
    </div>
  )
}
