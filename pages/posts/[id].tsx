import { useEffect } from 'react';
import supabase from '../../utils/supabase';

interface PostsModelInterface {
    posts: {
        id: number,
        created_at: Date,
        updated_at: Date,
        title: string,
        description: string,
        comments: any
    }
}

export async function getStaticPaths() {
    const { data: posts, error } = await supabase.from('posts').select('id');
    if (error) {
        throw new Error(error.message);
    }
    const paths = posts?.map((post) => ({
        params: { id: post.id.toString() },
    }))
    return { paths, fallback: false }
}
export async function getStaticProps({ params }: any) {
    const { data: posts, error } = await supabase.from('posts')
        .select('*,comments(*)')
        .eq('id', params.id);
    if (error) {
        throw new Error(error.message);
    }
    return {
        props: {
            posts,
        }
    }
}

const PostPage = ({ posts }: PostsModelInterface) => {
    useEffect(() => {
        supabase
            .channel('any')
            .on('postgres_changes', { event: 'UPDATE', schema: 'public', table: 'comments' }, payload => {
                console.log('Change received!', payload)
            })
            .subscribe();

    }, [])
    return (
        <div>
            <pre>{JSON.stringify(posts, null, 2)}</pre>
        </div>
    )
}

export default PostPage;