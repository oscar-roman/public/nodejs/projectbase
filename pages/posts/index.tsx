import { GetStaticPaths } from 'next';
import supabase from '../../utils/supabase';

interface PostsModelInterface {
  id: number,
  created_at: Date,
  updated_at: Date,
  title: string,
  description: string,
  comments: any
}

interface ListPostInterface {
  posts: PostsModelInterface[]
}


export async function getStaticProps() {
  const { data: posts, error } = await supabase.from('posts')
  .select('*,comments(*)');
  if (error) {
    throw new Error(error.message);
  }
  return {
    props: {
      posts,
    }
  }
}

const PostPage = ({ posts }: ListPostInterface) => {

  return (
    <div>
      <pre>{JSON.stringify(posts, null, 2)}</pre>
    </div>
  )
}

export default PostPage;