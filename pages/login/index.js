import supabase from '../../utils/supabase';
const LoginPage = () => {

    const handleSubmit = async (event) => {
        event.preventDefault();
        const email = event.target.email.value.trim();
        const password = event.target.password.value.trim();
        const { data, error } = await supabase.auth.signInWithPassword({
            email: email,
            password: password
        });
        console.log(data);
        if (error) {
            throw new Error(error.message);
        }
    }

    return (
        <div>
            <h1>Login</h1>
            <form onSubmit={handleSubmit} >
                <label htmlFor="email">Email</label>
                <input type="email" name="email" />
                <label htmlFor="password">Password</label>
                <input type="password" name="password" />
                <button type="submit">Enviar</button>
            </form>
        </div>
    );
}

export default LoginPage;